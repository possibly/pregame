import os
import sys
import subprocess

def home():
    print "----------------------------------" 
    cwd = workdir()
    
    name = raw_input("Name home directory: \n>")
    
    path = os.path.join(cwd,name)
    libpath = os.path.join(path,"lib")
    srcpath = os.path.join(path,"src")
    respath = os.path.join(path,"res")
    
    os.mkdir(path)
    os.mkdir(libpath)
    os.mkdir(srcpath)
    os.mkdir(respath)
    
    print path
    print libpath
    print srcpath
    print respath
    print "----------------------------------"

#constructing your own plist?
#maintaining keys between _description and _install
#checking dependencies so you dont run into errors!
#formatting values should be a module.
#configuring should be handled better then hardcoded.
def programs():
    cwd = workdir()
    os.chdir(cwd)
    
    plist_description = {
        "pip" : " -package manager for Python",
        "homebrew" : " -package manager for Mac",
        "cask" : " -extends homebrew",
        "virtualenv" : " -virtual environment handler for python",
        "virtualbox" : " -virtual machine handler",
        "vagrant" : " -create and configure portable dev environments",
        "node" : " -JavaScript platform for network applications",
        "yeoman, bower, grunt" : " -Scaffolds web apps, package manager, automates builds",
        "SASS" : " -CSS language extension",
        "compass" : " -framework for SASS",
        "git" : " -version control",
        "chrome" : " -web browser",
    }
    plist_install = {
        "pip" : "easy_install pip",
        "homebrew" : "ruby -e '$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)'",
        "cask" : "brew install caskroom/cask/brew-cask",
        "virtualenv" : "pip install virtualenv",
        "virtualbox" : "brew cask install virtualbox",
        "vagrant" : "brew cask install vagrant",
        "node" : "brew install node",
        "yeoman, bower, grunt" : "npm install -g yo",
        "SASS" : "gem install sass",
        "compass" : "gem install compass",
        "git" : "brew install git",
        "chrome" : "brew cask install google-chrome",
    }
    plist_req = {}
    plist_req["pip"] = plist_install["pip"]
    plist_req["homebrew"] = plist_install["homebrew"]
    plist_req["cask"] = plist_install["cask"]
    plist_req["node"] = plist_install["node"]
    
    print "----------------------------------"
    for key in plist_description:
        print key+""+plist_description[key]
    print ""
    confirm = raw_input("Install the above? \n>")
    if confirm == "yes":
        for key in plist_req:
            os.system(plist_req[key])
        for key in plist_install:
            if key in plist_req == False:
                os.system(plist_req[key])
    else:
        print "Type key of package you want to install"
        key = raw_input(">")
        
        while key in plist_install == False:
            print "Invalid."
            key = raw_input(">")
        
        subprocess.call(plist_install[key], shell=True)
    print "----------------------------------"

def workdir():
    print "Type yes or no"
    cwd = raw_input("The directory you want to set this up in is: "+os.getcwd()+"\n>")
    
    if cwd == "yes":
        cwd = os.getcwd()
    else: 
        valid = False
        while valid == False:
            cwd = raw_input("Where?: \n>")
            if os.access(cwd,os.F_OK) == False:
                print "Invalid."
            else:
                valid = True
    
    return cwd

args = {
    "-h": "establishes home directory",
    "-p": "installs programs",
}


if len(sys.argv) == 1:
    print "----------------------------------"
    print "WELCOME."
    print "Accepted arguments:"
    for key in args:
        print key+" "+args[key]
    print "----------------------------------"
else:
    for param in sys.argv:
        if param == "-h":
            home()
        elif param == "-p":
            programs()
            

